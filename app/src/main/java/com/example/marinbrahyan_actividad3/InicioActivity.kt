package com.example.marinbrahyan_actividad3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewAnimationUtils
import android.widget.Button
import androidx.constraintlayout.widget.ConstraintLayout

class InicioActivity : AppCompatActivity(),View.OnClickListener {

    var btnLogin:Button?=null
    var btnRegistro:Button?=null
    var clPantallaInicial:ConstraintLayout?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.inicio_activity)

        btnLogin=this.findViewById(R.id.btnLogin)
        btnRegistro=this.findViewById(R.id.btnRegistro)
        clPantallaInicial=this.findViewById(R.id.clPantallaInicial)

        btnLogin!!.setOnClickListener(this)
        btnRegistro!!.setOnClickListener(this)



    }
    //En funcion del boton pulsado inicia la actividad LoginActivity.kt
    //o la funcion RegisterActivity.kt
    override fun onClick(p0: View?) {
        if(p0==btnLogin){
            clPantallaInicial!!.visibility=View.GONE
            val intent:Intent= Intent(this, LoginActivity :: class.java)
            startActivity(intent)
            this.finish()
        }
        if(p0==btnRegistro){
            clPantallaInicial!!.visibility=View.GONE
            val intent:Intent= Intent(this, RegisterActivity :: class.java)
            startActivity(intent)
            this.finish()
        }

    }
}