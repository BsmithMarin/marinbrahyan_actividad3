package com.example.marinbrahyan_actividad3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    var etEmail:EditText?=null
    var etPassword:EditText?=null
    var btnAcceder:Button?=null
    var btnVolver:Button?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)

        etEmail=this.findViewById(R.id.etEmail)
        etPassword=this.findViewById(R.id.etPassword)
        btnAcceder=this.findViewById(R.id.btAcceder)
        btnVolver=this.findViewById(R.id.btnVolver)

        btnAcceder!!.setOnClickListener(this)
        btnVolver!!.setOnClickListener(this)
    }

    //Comprueba que el usuuario y contraseña coincida con los 2 únicos registrados en la apliacion
    // un Usuario y el administrador

    override fun onClick(v: View?) {

        var stEmail:String?=etEmail!!.text.toString().toLowerCase()
        var stPassword:String?=etPassword!!.text.toString().toLowerCase()

        if(v==btnAcceder){

            if(((stEmail=="usuario@usuario.com")&&(stPassword=="usuario"))||((stEmail=="admin@admin.com")&&(stPassword=="admin"))){
                val intent:Intent=Intent(this, MainActivityActivity::class.java)
                startActivity(intent)
                this.finish()
            }else{
                Toast.makeText(this,"Usuario o contraseña no validos", Toast.LENGTH_LONG).show()
            }
        }else if(v==btnVolver){
            val intent:Intent=Intent(this, InicioActivity::class.java)
            startActivity(intent)
            this.finish()
        }
    }

}