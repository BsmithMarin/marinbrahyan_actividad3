package com.example.marinbrahyan_actividad3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast

//Se usa para representar que ya se esta dentro de la apliación no hace nada significativo

class MainActivityActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        Toast.makeText(this,"Login correcto, bienvenido", Toast.LENGTH_LONG).show()

    }
}