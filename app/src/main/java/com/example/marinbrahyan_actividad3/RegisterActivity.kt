package com.example.marinbrahyan_actividad3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class RegisterActivity : AppCompatActivity(), View.OnClickListener {

    var etUsername:EditText?=null
    var etEmailRegister:EditText?=null
    var etPassword1:EditText?=null
    var etPassword2:EditText?=null
    var btnVolver2:Button?=null
    var btnRegistro2:Button?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.register_activity)

        etUsername=this.findViewById(R.id.etUserName)
        etEmailRegister=this.findViewById(R.id.etEmailRegister)
        etPassword1=this.findViewById(R.id.etPassword1)
        etPassword2=this.findViewById(R.id.etPassword2)
        btnVolver2=this.findViewById(R.id.btnVolver2)
        btnRegistro2=this.findViewById(R.id.btnRegistro2)

        btnRegistro2!!.setOnClickListener(this)
        btnVolver2!!.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        if(v==btnVolver2){
            val intent: Intent = Intent(this, InicioActivity::class.java)
            startActivity(intent)
            this.finish()
        }else if(v==btnRegistro2){

            //Guarda los textos de los objetos/elementos visuales en variables locales, para que sea
            //más fácil de leer el código

            var strUsername:String=etUsername!!.text.toString()
            var strEmail:String=etEmailRegister!!.text.toString()
            var strPassword1:String=etPassword1!!.text.toString()
            var strPassword2:String=etPassword2!!.text.toString()

            //Evalua en cascada que el Username tenga mas de 5 caracteres, que el Email sea valido
            //que las contraseñas coincidan, y que esta tengas mas de 8 caracteres, una mayuscula y minuscula
            //y al menos un numero

            if((UsernameValidator(strUsername))==false){
                Toast.makeText(this,"El nombre de usuario debe tener al menos 5 caracteres", Toast.LENGTH_LONG).show()
            }else if((EmailValidator(strEmail))==false) {
                Toast.makeText(this, "El Email introducido no es valido", Toast.LENGTH_LONG).show()
            }else if(strPassword2!=strPassword1){
                Toast.makeText(this,"Las contraseñas no coinciden", Toast.LENGTH_LONG).show()
            }else if((PasswordValidator(strPassword1))==false){
                Toast.makeText(this,"Las contraseña debe contener al menos una mayuscula, una minuscula y un numero", Toast.LENGTH_LONG).show()
            }else{
                Toast.makeText(this,"Registro completado con exito, revise su bandeja de entrada para validar el Email", Toast.LENGTH_LONG).show()
            }
        }
    }
    //Valida que las contraseña cumpla con los requisitos de tener al menos una mayuscula y una minuscula
    // 8 caracteres y al menos un numero
    fun PasswordValidator(Password:String):Boolean{

        var iContador:Int=0
        var bNumero:Boolean=false
        var bMayuscula:Boolean=false
        var bValida:Boolean=false
        var bMinuscula:Boolean=false

        for (i in Password){
            var caracter:String?=i.toString()
            if(caracter=="9"|| caracter=="8"|| caracter=="7"|| caracter=="6"|| caracter=="5"|| caracter=="4"|| caracter=="3"|| caracter=="2"|| caracter=="1"|| caracter=="0"){
                bNumero=true
            }
            if((caracter!=(caracter?.toLowerCase()))){
                bMayuscula=true
            }
            if((caracter!=(caracter?.toUpperCase()))){
                bMinuscula=true
            }
            iContador=iContador+1
        }
        if((bNumero==true)&&(bMayuscula==true)&&(bMinuscula==true)&&(iContador>=8)){
            bValida=true
        }
        return bValida
    }
    //Valida que el Email sea valido
    fun EmailValidator(Email:String):Boolean{

        var bValido:Boolean=false
        var bArroba:Boolean=false
        var bPunto:Boolean=false
        var iCuentaArrobas:Int=0

        //Evalua que despues de la arroba haya al menos un punto, para comprobar que el correo
        //Tenga un dominio, ademas previamente comprueba que haya una 'arroba'.
        //Además comprueba que solo haya una arroba, si hay más ta,bien dará por INVALIDO el Email

        for (i in Email){
            var caracter:String?=i.toString()
            if(caracter=="@"){
                bArroba=true
                iCuentaArrobas=iCuentaArrobas+1
            }

            if(iCuentaArrobas>1){
                bArroba=false
            }

            if((bArroba==true)&&(caracter==".")){
                bPunto=true
            }
        }
        if((bArroba==true)&&(bPunto==true)){
            bValido=true
        }

        return bValido
    }
    //valida que el Username sea de al menos 5 caracteres
    fun UsernameValidator(Username:String):Boolean{

        var iContador:Int=0

        for (i in Username){
            iContador=iContador+1
        }

        if(iContador>=5){
            return true
        }else{
            return false
        }
    }
}